#include "main.hpp"

Matrice::Matrice(std::vector<std::vector<int>>& pMat)
{
    matrice = pMat;

    for (int i = 0; i < TAILLE_I; i++)
    {
        colonnes.push_back(i);
    }

    for (int j = 0; j < TAILLE_J; j++)
    {
        lignes.push_back(j);
    }
}

bool Matrice::est_nulle()
{
    return (lignes.empty() || colonnes.empty());
}

// Nombre de 1 d'une colonne
int Matrice::nombre_de_1(int colonne)
{
    int somme = 0;
    std::vector<int>::iterator j;

    // Les itérations se font sur lignes et colonnes afin de
    // n'itérer que sur les lignes et colonnes actives
    for (j = lignes.begin(); j != lignes.end(); j++)
    {
        somme += matrice[colonne][*j];
    }

    return somme;
}

// Minimum de 1 dans les colonnes de la matrice
int Matrice::minimum()
{
    int min = TAILLE_I;
    int somme;

    std::vector<int>::iterator i;

    for (i = colonnes.begin(); i != colonnes.end(); i++)
    {
        somme = nombre_de_1(*i);

        if (somme < min)
        {
            min = somme;
        }
    }

    return min;
}

/*
Implémentation de l'algorithme X de Knuth

Prend en entrée un pointeur sur une matrice m qui représente un problème
de couverture exacte.

Affiche les lignes correspondant aux sous ensembles retenus dans la solution.

Cet algorithme ne trouve pas nécessairement la solution (par exemple si la
première ligne sélectionnée n'est pas dans la solution). Dans ce cas, 
l'algorithme affichera "echec".

Le résumé de l'algorithme sous forme d'actions à effectuer est transcrit
dans les commentaires.

/!\ matrice[colonne][ligne] ; i = colonne et j = ligne /!\
*/
int algo_x(Matrice* m)
{
    // Tant que la matrice n'est pas vide
    while (!m->est_nulle())
    {
        //Choisir la pemière colonne c contenant un minimum de 1
        // -> trouver le minimum de 1 [si min = 0; échec]
        int min = m->minimum();

        if (min == 0)
        {
            std::cout << "échec" << std::endl;
            return 1;
        }
        
        // -> prendre la 1ère colonne avec ce minimum

        int indice_colonne = 0;

        while (m->nombre_de_1(m->colonnes[indice_colonne]) > min)
        {
            indice_colonne ++;
        }

        int colonne = m->colonnes[indice_colonne];
        
        // Choisir une ligne l qui a 1 pour cette colonne

        int indice_ligne = 0;

        while (m->matrice[colonne][m->lignes[indice_ligne]] != 1)
        {
            indice_ligne ++;
        }

        int ligne = m->lignes[indice_ligne];

        // Ajouter l à la solution partielle -> print l

        std::cout << "ligne : " << ligne << std::endl;

        std::vector<int>::iterator i, j;

        // for i tq matrice[i][l] = 1
        for (i = m->colonnes.begin(); i != m->colonnes.end(); i++)
        {
            if (m->matrice[*i][ligne] == 1)
            {
                
                // for j tq matrice [i][j] = 1
                for (j = m->lignes.begin(); j != m->lignes.end(); j++)
                {
                    if (m->matrice[*i][*j] == 1)
                    {
                        
                        // Supprimer ligne j de matrice
                        m->lignes.erase(j);
                        j--;    // revenir en arrière, comme il y 
                                // a une ligne de moins
                        
                    }
                }
                // Supprimer colonne i de matrice
                m->colonnes.erase(i); 
                i--;  // revenir en arrière, comme il y a une colonne de moins
            }
        }

    }

    //succès
    std::cout << "succes" << std::endl;
    return 0;
}

int main()
{
    /*
    Exemple de matrice pour le problème suivant : 

    Ensemble : {1, 2, 3}

    Sous-ensembles : 
    A : {1}     (ligne 0)
    B : {1, 2}  (ligne 1)
    C : {2, 3}  (ligne 2)
    
    La matrice obtenue est donc : 

    1 0 0
    1 1 0
    0 1 1

    Où les colonnes correspondent aux éléments de l'ensemble et les lignes,
    aux sous-ensembles.

    Le résultat de ce problème sont les ensembles A et C (soit les lignes 0 et 2)

    Le format de matrice de cet algorithme est m[colonne][ligne], ainsi 
    la matrice est rentrée de manière transposée. 

    /!\ Ne pas oublier de modifier TAILLE_I et TAILLE_J en cas de matrice
    d'une autre taille /!\
    */ 

    std::vector<std::vector<int>> m_arg = {{1, 1, 0}, {0, 1, 1}, {0, 0, 1}};

    Matrice* m = new Matrice(m_arg);

    return algo_x(m);
}
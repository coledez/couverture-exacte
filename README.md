# Couverture Exacte

Implémentation de l'algorithme X de Knuth pour résoudre le problème de couverture exacte : 

On dispose d'un ensemble, et de plusieurs sous-ensembles. Le but est de choisir l'ensemble de sous-ensembles approprié afin que chaque élément de l'ensemble soit dans un et un seul sous-ensemble. 
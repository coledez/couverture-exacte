#include <vector>
#include <iostream>
#define TAILLE_I 3
#define TAILLE_J 3

class Matrice
{
    // On stocke la matrice sous la forme d'une matrice qui ne servira qu'à
    // la lecture, et des listes des lignes et colonnes, qui serviront à 
    // supprimer facilement les lignes et colonnes.
    
    public:
    std::vector<std::vector<int>> matrice;
    std::vector<int> lignes;
    std::vector<int> colonnes;

    Matrice( std::vector<std::vector<int>>&);

    bool est_nulle();
    int nombre_de_1(int colonne);
    int minimum();
};